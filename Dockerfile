# Start with a base image containing Java runtime
FROM openjdk:8-jdk-alpine



# Add a volume pointing to /tmp
VOLUME /tmp


# The application's jar file
ARG JAR_FILE=target/producer.jar

# Add the application's jar to the container
ADD ${JAR_FILE} producer.jar

# Run the jar file 
ENTRYPOINT exec java $JAVA_OPTS -jar /producer.jar
#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/producer.jar"]
