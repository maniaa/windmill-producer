package com.windmill.producer.dto;
import com.windmill.producer.model.WindMill;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ReportResDto {
	
	private Double sum;
	
	private Double min;
	
	private Double max;
	
	private Double average;
	
	private WindMill mill;
	
	private Boolean isActive;
	
	public ReportResDto(Double sum, Double min,Double max,Double average, WindMill mill) {
		super();
		this.sum = sum;
		this.min = min;
		this.max = max;
		this.average = average;
        this.mill = mill;
	}
	
	public ReportResDto() {
		
	}

}
