package com.windmill.producer.dto;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class RegisterReqDto {
	
	@NotNull(message = "Name To Cannot be Empty")
	private String name;
	
	@NotNull(message = "Address To Cannot be Empty")
	private String address;
	
	@NotNull(message = "Latitude To Cannot be Empty")
	private String latitude;
	
	@NotNull(message = "Longitude To Cannot be Empty")
	private String longitude;

}
