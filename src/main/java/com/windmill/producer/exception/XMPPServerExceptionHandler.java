package com.windmill.producer.exception;

public class XMPPServerExceptionHandler extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4108970394919771532L;

	public XMPPServerExceptionHandler(String message) {
		super(message);
	}

}
