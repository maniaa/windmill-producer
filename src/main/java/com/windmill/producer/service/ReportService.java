package com.windmill.producer.service;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.windmill.producer.dto.ReportResDto;
import com.windmill.producer.exception.NoDataFoundException;
import com.windmill.producer.repo.ElectricityRepo;
import com.windmill.producer.response.ListResponse;
import com.windmill.producer.util.ProducerConstant;

@Service
public class ReportService {
	
	@Autowired
	ElectricityRepo electricityRepo;

	public ListResponse<List<ReportResDto>> getElectricityDetails() {
		ListResponse<List<ReportResDto>> response = new ListResponse<>();
		List<ReportResDto> details = new ArrayList<>();
		List<ReportResDto> electricity = electricityRepo.findReportByDay();
		if(electricity == null || electricity.isEmpty()) {
			throw new NoDataFoundException(ProducerConstant.NOT_AVAILABLE);
		}
		electricity.forEach(c -> details.add(c));
		response.setData(details);
		response.setStatus(HttpServletResponse.SC_OK);
		response.setMessage(ProducerConstant.DATA_RETRIEVED);
		return response;
	}

}
