package com.windmill.producer.service;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.windmill.producer.dto.RegisterReqDto;
import com.windmill.producer.model.WindMill;
import com.windmill.producer.repo.WindMillRepo;
import com.windmill.producer.response.Response;
import com.windmill.producer.util.ProducerConstant;
import com.windmill.producer.util.Util;

@Service
public class RegisterService {
	
	@Autowired
	WindMillRepo windMillRepo;
	

	public Response<String> addWindMill(RegisterReqDto registerReqDto) {
		WindMill windMill = new WindMill();
		windMill.setId(randomSerialNo());
		windMill.setName(registerReqDto.getName());
		windMill.setAddress(registerReqDto.getAddress());
		windMill.setLatitude(registerReqDto.getLatitude());
		windMill.setLongitude(registerReqDto.getLatitude());
		windMill.setCreatedAt(Util.getCurrentDateTime());
		windMillRepo.save(windMill);
		Response<String> response = new Response<>();
		response.setStatus(HttpServletResponse.SC_OK);
		response.setMessage(ProducerConstant.DATA_RETRIEVED);
		return response;
	}
	
	private String randomSerialNo() {
		    String id;
		    WindMill mill = windMillRepo.findTopByOrderByCreatedAtDesc();
			if(mill != null ) {
				id =  Util.randomId(mill.getId());				
			}else {
				id = Util.randomId(null);				
			}
			return id;
			
	}

}
