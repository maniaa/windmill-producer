package com.windmill.producer.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class ApplicationProperties {

	@Autowired
	private Environment env;

	public String getMessageQueue() {
		return env.getProperty("api.message.exchange");
	}

}
