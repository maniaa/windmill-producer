package com.windmill.producer.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.windmill.producer.dto.ReportResDto;
import com.windmill.producer.response.ListResponse;
import com.windmill.producer.service.ReportService;

@RestController
@RequestMapping("/reports")
public class ReportController {
	
	@Autowired
	private ReportService reportService;
	
	@GetMapping
	public @ResponseBody ListResponse<List<ReportResDto>> getElectricityDetails() {
		return reportService.getElectricityDetails();
	}

}
