package com.windmill.producer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.windmill.producer.dto.RegisterReqDto;
import com.windmill.producer.response.Response;
import com.windmill.producer.service.RegisterService;

@RestController
@RequestMapping("/register")
public class RegisterController {

	@Autowired
	private RegisterService registerService;

	@PostMapping
	public @ResponseBody Response<String> addWindMill(@RequestBody RegisterReqDto registerReqDto) {
		return registerService.addWindMill(registerReqDto);
	}

}
