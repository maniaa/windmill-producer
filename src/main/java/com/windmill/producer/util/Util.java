package com.windmill.producer.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.security.SecureRandom;

import org.springframework.data.domain.PageRequest;

public class Util {
	
	static final String AB = "abcdefghijklmnopqrstuvwxyz";
	static SecureRandom rnd = new SecureRandom();

	private Util() {

	}
	
	public static PageRequest getPagination(Integer page, Integer size) {
		return PageRequest.of(page != null ? page : 0, size != null ? size : 10);
	}
	
	public static String getCurrentDateTimeString() {
		return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
	}

	public static LocalDateTime getCurrentDateTime() {
		return LocalDateTime.now();
	}

	public static LocalDateTime getSessionExpiryTime() {
		return LocalDateTime.now().plusHours(1);
	}

	public static LocalDateTime getLocalDateTimeFromString(String time) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		return LocalDateTime.parse(time, formatter);
	}
	
	public static String randomString(int len) {
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		return sb.toString();
	}
	
	public static String randomId(String input) {
		int count = 1;
		Integer num = 1;
		if (input != null) {
			num = getTrailingInteger(input) + 1;
			count = Integer.toString(num).length();
		}
		String str = randomString(16 - count);
		Pattern p = Pattern.compile("[0-15]+");
		Matcher m = p.matcher(str + num);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			int minLength = m.group().length();
			String format = "%0" + minLength + "d";
			String incrementedNumber = String.format(format, Integer.parseInt(m.group()));
			m.appendReplacement(sb, incrementedNumber);
		}

		m.appendTail(sb);
		return sb.toString();
	}

	public static int getTrailingInteger(String str) {
		int positionOfLastDigit = getPositionOfLastDigit(str);
		if (positionOfLastDigit == str.length()) {
			return -1;
		}
		return Integer.parseInt(str.substring(positionOfLastDigit));
	}

	public static int getPositionOfLastDigit(String str) {
		int pos;
		for (pos = str.length() - 1; pos >= 0; --pos) {
			char c = str.charAt(pos);
			if (!Character.isDigit(c))
				break;
		}
		return pos + 1;
	}

}