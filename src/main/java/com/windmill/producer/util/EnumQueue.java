package com.windmill.producer.util;

public enum EnumQueue {

	MESSAGE("api_message");

	private String queue;

	EnumQueue(String queue) {
		this.queue = queue;
	}

	public String getQueue() {
		return queue;
	}

}
