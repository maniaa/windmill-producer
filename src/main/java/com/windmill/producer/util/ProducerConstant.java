
package com.windmill.producer.util;

/**
 * A container to store static constants Strings for proper code sequence. This
 * Constants are declared as final so it cannot be changed. These constants will
 * not be changed on the runtime.
 * 
 */

public class ProducerConstant {

	public static final String DATA_RETRIEVED = "Data retrieved successfully";
	
	public static final String DATA_ADDED = "Data added successfully";
	
	public static final String NOT_AVAILABLE = "No Records Found";

	private ProducerConstant() {
	}
}
