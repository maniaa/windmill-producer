package com.windmill.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Application {

	/**
	 * This main class act as an starting point of all application.
	 * 
	 * @param args array.
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
