package com.windmill.producer.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.windmill.producer.dto.ReportResDto;
import com.windmill.producer.model.Electricity;;

@Repository
public interface ElectricityRepo extends JpaRepository<Electricity, Long> {
	
	@Query("SELECT new com.windmill.producer.dto.ReportResDto(SUM(m.electricityCharge),MIN(m.electricityCharge),MAX(m.electricityCharge)"
			+ ",AVG(m.electricityCharge),m.mill)"
			+ "FROM Electricity m GROUP BY DAY(DATE(FROM_UNIXTIME(m.createdAt)))")
	public List<ReportResDto> findReportByDay();

}
