package com.windmill.producer.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.windmill.producer.model.WindMill;

@Repository
public interface WindMillRepo extends JpaRepository<WindMill, String> {

	WindMill findTopByOrderByCreatedAtDesc();
	

}
