package com.windmill.producer.schedular;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.windmill.producer.model.WindMill;
import com.windmill.producer.queue.PushMessagePayload;
import com.windmill.producer.queue.QueueService;
import com.windmill.producer.repo.WindMillRepo;
import com.windmill.producer.util.Util;

@Component
public class PayloadSchedular {
	
	@Autowired
	QueueService queueService;
	
	@Autowired
	WindMillRepo windMillRepo;
	
	@Scheduled(fixedDelay = 30000)
	public void pushPayload() {
		for (WindMill m : windMillRepo.findAll()) {
			PushMessagePayload pushMessagePayload = new PushMessagePayload(m.getId(), 5d, Util.getCurrentDateTimeString());
			//queueService.sendPushMessagePayload(pushMessagePayload);
		}
	}

}
