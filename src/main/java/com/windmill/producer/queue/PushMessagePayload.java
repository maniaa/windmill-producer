package com.windmill.producer.queue;

import lombok.Data;

@Data
public class PushMessagePayload {

	private String millId;

	private Double electricityCharge;

	private String datetime;

	public PushMessagePayload(String millId, Double electricityCharge, String datetime) {
		super();
		this.millId = millId;
		this.electricityCharge = electricityCharge;
		this.datetime = datetime;
	}

}
