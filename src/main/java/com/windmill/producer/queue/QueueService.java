package com.windmill.producer.queue;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.windmill.producer.config.ApplicationProperties;
import com.windmill.producer.util.EnumQueue;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class QueueService {

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private ApplicationProperties applicationProperties;

	public boolean sendPushMessagePayload(PushMessagePayload pushMessagePayload) {
		try {
			rabbitTemplate.convertAndSend(applicationProperties.getMessageQueue(), EnumQueue.MESSAGE.getQueue(),
					pushMessagePayload);
		} catch (AmqpException a) {
			log.error("Error in Sending to RabbitMQ Message Exchange", a);
			return false;
		} catch (Exception a) {
			log.error("Error in Sending to Message Queue", a);
			return false;

		}
		return true;
	}
}
